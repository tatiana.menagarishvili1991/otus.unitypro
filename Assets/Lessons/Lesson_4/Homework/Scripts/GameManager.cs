using System.Collections.Generic;
using NaughtyAttributes;
using UnityEngine;

namespace Lessons.Lesson_4.Homework.Scripts
{
    public enum GameState
    {
        Off,
        Preparing,
        Playing,
        Paused,
        Finished
    }

    public class GameManager : MonoBehaviour
    {
        private readonly List<IGameListener> _listeners = new();
        private readonly List<IGameUpdateListener> _updateListeners = new();

        public GameState State { get; private set; } = GameState.Off;


        private void Update()
        {
            if (State != GameState.Playing) return;

            var deltaTime = Time.deltaTime;
            foreach (var updateListener in _updateListeners)
            {
                updateListener.OnGameUpdate(deltaTime);
            }
        }

        public void AddListener(IGameListener listener)
        {
            if (listener == null) return;

            _listeners.Add(listener);

            if (listener is IGameUpdateListener updateListener)
            {
                _updateListeners.Add(updateListener);
            }
        }

        public void RemoveListener(IGameListener listener)
        {
            if (listener == null) return;

            _listeners.Remove(listener);

            if (listener is IGameUpdateListener updateListener)
            {
                _updateListeners.Remove(updateListener);
            }
        }

        [Button("Prepare")]
        public void PrepareGame()
        {
            foreach (var listener in _listeners)
            {
                if (listener is IGamePrepareListener prepareListener)
                {
                    prepareListener.OnPrepareGame();
                }
            }
            
            State = GameState.Preparing;
        }

        [Button("Start")]
        public void StartGame()
        {
            foreach (var listener in _listeners)
            {
                if (listener is IGameStartListener startListener)
                {
                    startListener.OnStartGame();
                }
            }

            State = GameState.Playing;
        }

        [Button("Pause")]
        public void PauseGame()
        {
            foreach (var listener in _listeners)
            {
                if (listener is IGamePauseListener pauseListener)
                {
                    pauseListener.OnPauseGame();
                }
            }

            State = GameState.Paused;
        }

        [Button("Resume")]
        public void ResumeGame()
        {
            foreach (var listener in _listeners)
            {
                if (listener is IGameResumeListener resumeListener)
                {
                    resumeListener.OnResumeGame();
                }
            }

            State = GameState.Playing;
        }

        [Button("Finish")]
        public void FinishGame()
        {
            foreach (var listener in _listeners)
            {
                if (listener is IGameFinishListener finishListener)
                {
                    finishListener.OnFinishGame();
                }
            }

            State = GameState.Finished;
        }
    }
}