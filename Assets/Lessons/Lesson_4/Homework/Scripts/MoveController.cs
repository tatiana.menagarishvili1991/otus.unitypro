﻿using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

namespace Lessons.Lesson_4.Homework.Scripts
{
    public class MoveController : MonoBehaviour, IGameStartListener, IGamePauseListener, IGameResumeListener, IGameFinishListener
    {
        [SerializeField] private KeyboardInput _input;
        [SerializeField] private GameObject _player;
        [SerializeField] private List<Transform> _playerPoints;

        private int _currentPlayerPoint = 1;

        void IGameStartListener.OnStartGame()
        {
            _input.OnInput += OnMove;
        }

        void IGamePauseListener.OnPauseGame()
        {
            _input.OnInput -= OnMove;
        }

        void IGameResumeListener.OnResumeGame()
        {
            _input.OnInput += OnMove;
        }

        void IGameFinishListener.OnFinishGame()
        {
            _input.OnInput -= OnMove;
        }

        private void OnMove(InputType inputType)
        {
            var newPlayerPoint = _currentPlayerPoint + (inputType is InputType.Left ? -1 : 1);
            if (newPlayerPoint >= 0 && newPlayerPoint <= 2)
            {
                _currentPlayerPoint = newPlayerPoint;
                _player.transform.DOMove(_playerPoints[newPlayerPoint].position, 0.3f);
            }
        }
    }
}