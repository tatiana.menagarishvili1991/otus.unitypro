﻿using System.Collections.Generic;
using UnityEngine;
using Random = System.Random;

namespace Lessons.Lesson_4.Homework.Scripts.Objects
{
    public class RoadPart : MonoBehaviour
    {
        [SerializeField] private Transform _obstacleParent;
        [SerializeField] private Transform _obstaclePrefab;
        [SerializeField] private List<Transform> _points;
        private readonly List<Transform> _obstacles = new();

        private Vector3 _defaultPosition;

        private void Awake()
        {
            _defaultPosition = transform.position;
        }

        public void GenerateObstacles()
        {
            if (_obstacles.Count == 0)
            {
                for (int i = 0; i < 10; i++)
                {
                    var obstacle = Instantiate(_obstaclePrefab, _obstacleParent);
                    obstacle.localPosition = new Vector3(0, 0, i - 4);
                    _obstacles.Add(obstacle);
                }
            }

            for (int i = 0; i < 10; i++)
            {
                var obstacle = _obstacles[i];
                var index = new Random().Next(0, 3);
                var position = obstacle.localPosition;
                position.x = _points[index].localPosition.x;
                obstacle.localPosition = position;
            }
        }

        public void Reset()
        {
            _obstacles.ForEach(t => Destroy(t.gameObject));
            _obstacles.Clear();
            transform.position = _defaultPosition;
        }
    }
}