﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Lessons.Lesson_4.Homework.Scripts
{
    public class TimerManager : MonoBehaviour, IGameUpdateListener
    {
        private List<Timer> _timers = new();
        private List<Timer> _globalTimers = new();

        public void StartTimer(float duration, Action<float> onUpdate, Action onFinish, bool isGlobal = false)
        {
            var timer = new Timer(duration, onUpdate, onFinish);
            if (isGlobal)
            {
                _globalTimers.Add(timer);
            }
            else
            {
                _timers.Add(timer);
            }
            timer.Start(Time.time);
        }

        private void Update()
        {
            UpdateTimers(_globalTimers);
        }

        void IGameUpdateListener.OnGameUpdate(float deltaTime)
        {
            UpdateTimers(_timers);
        }

        private void UpdateTimers(List<Timer> timers)
        {
            List<Timer> timersForRemove = new List<Timer>();

            timers.ForEach(timer =>
            {
                if (!UpdateTimer(timer, Time.time))
                {
                    timersForRemove.Add(timer);
                }
            });

            timersForRemove.ForEach(timer => timers.Remove(timer));
        }

        private bool UpdateTimer(Timer timer, float time)
        {
            if (time - timer.StartTime < timer.Duration)
            {
                timer.Update(time);
                return true;
            }

            timer.Finish();
            return false;
        }
    }

    internal class Timer
    {
        public float Duration;
        public float StartTime;
        public Action<float> OnUpdate;
        public Action OnFinish;

        public bool IsActive { get; private set; }

        public Timer(float duration, Action<float> onUpdate, Action onFinish)
        {
            Duration = duration;
            OnUpdate = onUpdate;
            OnFinish = onFinish;
        }

        public void Start(float time)
        {
            IsActive = true;
            StartTime = Time.time;
        }

        public void Update(float time)
        {
            OnUpdate?.Invoke(time - StartTime);
        }

        public void Finish()
        {
            IsActive = false;
            OnFinish?.Invoke();
        }
    }
}