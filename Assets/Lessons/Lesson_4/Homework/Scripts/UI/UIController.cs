﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Lessons.Lesson_4.Homework.Scripts.UI
{
    public class UIController : MonoBehaviour, IGameStartListener, IGamePauseListener, IGameResumeListener, IGameFinishListener, IGamePrepareListener
    {
        [SerializeField] private GameManager _gameManager;

        [SerializeField] private Button _startButton;
        [SerializeField] private Button _pauseButton;
        [SerializeField] private Button _resumeButton;
        [SerializeField] private GameObject _finishScore;
        [SerializeField] private TMP_Text _countdown;
        
        private readonly int _countdownDuration = 3;

        private void Awake()
        {
            _startButton.onClick.AddListener(OnStartButtonClick);
            _pauseButton.onClick.AddListener(OnPauseButtonClick);
            _resumeButton.onClick.AddListener(OnResumeButtonClick);
        }


        private void OnStartButtonClick()
        {
            _gameManager.PrepareGame();
        }

        public void UpdateCountdown(float pastTime)
        {
            _countdown.text = (_countdownDuration - (int) pastTime).ToString();
        }

        private void OnPauseButtonClick()
        {
            _gameManager.PauseGame();
        }

        private void OnResumeButtonClick()
        {
            _gameManager.ResumeGame();
        }

        public void OnPrepareGame()
        {
            _finishScore.gameObject.SetActive(false);
            _startButton.gameObject.SetActive(false);
            _countdown.gameObject.SetActive(true);
        }

        void IGameStartListener.OnStartGame()
        {
            _countdown.gameObject.SetActive(false);
            _pauseButton.gameObject.SetActive(true);
        }

        void IGamePauseListener.OnPauseGame()
        {
            _pauseButton.gameObject.SetActive(false);
            _resumeButton.gameObject.SetActive(true);
        }

        void IGameResumeListener.OnResumeGame()
        {
            _pauseButton.gameObject.SetActive(true);
            _resumeButton.gameObject.SetActive(false);
        }

        void IGameFinishListener.OnFinishGame()
        {
            _pauseButton.gameObject.SetActive(false);
            _finishScore.gameObject.SetActive(true);
            _startButton.gameObject.SetActive(true);
        }
    }
}