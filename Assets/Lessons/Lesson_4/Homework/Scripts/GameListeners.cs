﻿namespace Lessons.Lesson_4.Homework.Scripts
{
    public interface IGameListener
    {
    }

    public interface IGamePrepareListener : IGameListener
    {
        void OnPrepareGame();
    }

    public interface IGameStartListener : IGameListener
    {
        void OnStartGame();
    }

    public interface IGamePauseListener : IGameListener
    {
        void OnPauseGame();
    }

    public interface IGameResumeListener : IGameListener
    {
        void OnResumeGame();
    }

    public interface IGameFinishListener : IGameListener
    {
        void OnFinishGame();
    }

    public interface IGameUpdateListener : IGameListener
    {
        void OnGameUpdate(float deltaTime);
    }
}