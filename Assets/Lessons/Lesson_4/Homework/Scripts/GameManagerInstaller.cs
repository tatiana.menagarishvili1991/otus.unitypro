﻿using UnityEngine;

namespace Lessons.Lesson_4.Homework.Scripts
{
    [RequireComponent(typeof(GameManager))]
    public class GameManagerInstaller : MonoBehaviour
    {
        private void Awake()
        {
            var gameManager = GetComponent<GameManager>();
            var listeners = GetComponentsInChildren<IGameListener>();

            foreach (var listener in listeners)
            {
                gameManager.AddListener(listener);
            }
        }
    }
}