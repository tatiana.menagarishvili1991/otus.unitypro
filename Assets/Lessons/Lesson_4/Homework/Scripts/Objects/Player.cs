﻿using System;
using UnityEngine;

namespace Lessons.Lesson_4.Homework.Scripts.Objects
{
    public class Player : MonoBehaviour
    {
        public event Action RoadTriggered;
        public event Action ObstacleCollided;

        private void OnTriggerEnter(Collider other)
        {
            if (other.name == "RoadTrigger")
            {
                RoadTriggered?.Invoke();
            }
            else
            {
                ObstacleCollided?.Invoke();
            }
        }
    }
}