﻿using Lessons.Lesson_4.Homework.Scripts.UI;
using UnityEngine;

namespace Lessons.Lesson_4.Homework.Scripts
{
    public class GameStartController : MonoBehaviour, IGamePrepareListener
    {
        [SerializeField] private GameManager _gameManager;
        [SerializeField] private UIController _uiController;
        [SerializeField] private TimerManager _timerManager;

        private readonly int _countdownDuration = 3;
        
        void IGamePrepareListener.OnPrepareGame()
        {
            _timerManager.StartTimer(_countdownDuration, _uiController.UpdateCountdown, _gameManager.StartGame, true);
        }
    }
}