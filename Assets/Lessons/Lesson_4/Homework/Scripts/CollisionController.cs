﻿using Lessons.Lesson_4.Homework.Scripts.Objects;
using UnityEngine;

namespace Lessons.Lesson_4.Homework.Scripts
{
    public class CollisionController : MonoBehaviour, IGameStartListener, IGameFinishListener, IGamePauseListener, IGameResumeListener
    {
        [SerializeField] private GameManager _gameManager;
        [SerializeField] private RoadController _roadController;
        [SerializeField] private Player _player;

        private void StartObserveCollisions()
        {
            _player.RoadTriggered += _roadController.OnRoadTrigger;
            _player.ObstacleCollided += _gameManager.FinishGame;
        }

        private void StopObserveCollisions()
        {
            _player.RoadTriggered -= _roadController.OnRoadTrigger;
            _player.ObstacleCollided -= _gameManager.FinishGame;
        }

        void IGameStartListener.OnStartGame()
        {
            StartObserveCollisions();
        }

        void IGameFinishListener.OnFinishGame()
        {
            StopObserveCollisions();
        }

        void IGamePauseListener.OnPauseGame()
        {
            StopObserveCollisions();
        }

        void IGameResumeListener.OnResumeGame()
        {
            StartObserveCollisions();
        }
    }
}