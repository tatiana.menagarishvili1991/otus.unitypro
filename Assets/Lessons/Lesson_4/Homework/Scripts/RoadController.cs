﻿using System.Collections.Generic;
using System.Linq;
using Lessons.Lesson_4.Homework.Scripts.Objects;
using UnityEngine;

namespace Lessons.Lesson_4.Homework.Scripts
{
    public class RoadController : MonoBehaviour, IGameStartListener, IGamePauseListener, IGameResumeListener, IGameFinishListener,
        IGameUpdateListener, IGamePrepareListener
    {
        [SerializeField] private List<RoadPart> _roadParts;
        [SerializeField] private float _speed;

        private readonly Queue<RoadPart> _queue = new Queue<RoadPart>();
        private bool _isPlaying = false;

        void IGamePrepareListener.OnPrepareGame()
        {
            _queue.Clear();
            _roadParts.ForEach(part =>
            {
                part.Reset();
                _queue.Enqueue(part);
            });
            _roadParts.Last().GenerateObstacles();
        }
        
        void IGameStartListener.OnStartGame()
        {
            _isPlaying = true;
        }

        void IGamePauseListener.OnPauseGame() => _isPlaying = false;

        void IGameResumeListener.OnResumeGame() => _isPlaying = true;

        void IGameFinishListener.OnFinishGame()
        {
            _isPlaying = false;
        }

        public void OnGameUpdate(float deltaTime)
        {
            if (_isPlaying)
            {
                var moveDelta = _speed * deltaTime;
                _roadParts.ForEach(roadPart =>
                {
                    var pos = roadPart.transform.position;
                    pos.z -= moveDelta;
                    roadPart.transform.position = pos;
                });
            }
        }

        public void OnRoadTrigger()
        {
            var part = _queue.Dequeue();
            var newPos = _queue.Peek().transform.position;
            newPos.z += 60;
            part.transform.position = newPos;
            _queue.Enqueue(part);
            part.GenerateObstacles();
        }
    }
}