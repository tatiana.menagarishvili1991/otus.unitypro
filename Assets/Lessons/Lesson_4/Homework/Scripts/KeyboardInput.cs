﻿using System;
using UnityEngine;

namespace Lessons.Lesson_4.Homework.Scripts
{
    public enum InputType
    {
        Left,
        Right
    }

    public class KeyboardInput : MonoBehaviour, IGameUpdateListener
    {
        public Action<InputType> OnInput;

        void IGameUpdateListener.OnGameUpdate(float deltaTime)
        {
            HandleKeyboard();
        }

        private void HandleKeyboard()
        {
            if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                OnInput(InputType.Left);
            }
            else if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                OnInput(InputType.Right);
            }
        }
    }
}